
Simple project for controlling ws2812b stripes with Atmega328p.

Per default the strip is connected on +5V and GND, and data-in from PORTB PB2, on Arduino Digital Pin 10.
On default the number of LEDs is set to 64, can be increased to 1024 or more.

Setting the LEDs:

Connect to Atmega over UART. Default is 9600 Baud.

To send the RGB values the bits have to be repacked.

instead of three bytes for 3*8bit RGB values:
```
//bits..
rval = rrrrrrrr;
gval = gggggggg;
bval = bbbbbbbb;
```
repack the bits to for bytes:
```
byte1=01rrrrrr;
byte2=01rrgggg;
byte3=01ggggbb;
byte4=01bbbbbb;
```

pseudo code:
```
byte1=64|(rval >>2);
byte2=64|((rval&3)<<4)|(gval>>4);
byte3=64|((gval&15)<<2)|(bval>>6);
byte4=64|(bval&63);
```

special chars: the dot '.' is marker for "start from beginning", the exclmation mark clears all LEDs and starts from beginning.



