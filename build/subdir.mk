# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../light_ws2812.c \
../main.c \
../ringuart.c 

OBJS += \
./light_ws2812.o \
./main.o \
./ringuart.o 

C_DEPS += \
./light_ws2812.d \
./main.d \
./ringuart.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


