/*
 * main.c
 *
 */

#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "ringuart.h"
#define ws2812_port B
#define ws2812_pin 2
#include "light_ws2812.h"
#define LEDcount 64
struct cRGB led[64];

uint8_t haschanged = 0;
uint16_t ledcounter = 0;
uint8_t bytecounter = 0;

int main(void) {
	ringuartinit();
	PORTB |= (1 << ws2812_pin);
	txring_enqueue('l');
	txring_enqueue('e');
	txring_enqueue('d');
	txring_enqueue('\r');
	txring_enqueue('\n');
	uint8_t lastval = 0;
	uint8_t newval = 0;
	uint8_t rval = 0;
	uint8_t gval = 0;
	uint8_t bval = 0;
	while (1) {

		while (rxring_count()) {
			lastval = newval;
			newval = rxring_dequeue();
			if (newval == '!') {
				// '!' is clear screen
				for (uint16_t i = 0; i < LEDcount; i++) {
					led[i].r = 0;
					led[i].g = 0;
					led[i].b = 0;

				}
				ledcounter = 0;
				bytecounter = 0;
				haschanged = 1; //now send data
				txring_enqueue('O');
				txring_enqueue('K');
				txring_enqueue('\r');
				txring_enqueue('\n');
			} else if (newval == '.') {
				// '.' is start and stop
				//reset all
				ledcounter = 0;
				bytecounter = 0;
				txring_enqueue('o');
				txring_enqueue('k');
				txring_enqueue('\r');
				txring_enqueue('\n');
			} else {
				//txring_enqueue(newval);
				if ((newval & 64) && (ledcounter < LEDcount)) {
					//txring_enqueue('.');
					switch (bytecounter) {
					case 0:
						//nothing to do but remember the first byte
						break;
					case 1:
						rval = ((lastval & 63) << 2) | ((newval >> 4) & 3);
						break;
					case 2:
						gval = ((lastval & 15) << 4) | ((newval >> 2) & 15);
						break;
					case 3:
						bval = ((lastval & 3) << 6) | (newval & 63);
						break;
					default:
						break;
					}
					bytecounter++;
					if (bytecounter > 3) {
						//txring_enqueue('!');
						bytecounter = 0;
						haschanged = 1; //now at least one RGB-dataset arrived
						led[ledcounter].r = rval;
						led[ledcounter].g = gval;
						led[ledcounter].b = bval;
						ledcounter++;
					}
				} else {
					//error.. something went wrong, ignore values from now on..
					ledcounter = LEDcount + 1;
				}
			}
		}
		if (haschanged) {
			ws2812_setleds(led, LEDcount);
			haschanged = 0;
		}
	}
}
