/*
 * ringuart.h
 *
 *      minicom -b 9600 -D /dev/ttyAMA0
 *      besser: putty
 *      oder: screen /dev/ttyUSB3 9600
 *      	beenden mit Ctrl-a K y
 */

#ifndef RINGUART_H_
#define RINGUART_H_
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

//bitsize should be between 2 and 7
#define RINGUART_RX_BUFFER_BITSIZE 7
#define RINGUART_TX_BUFFER_BITSIZE 7
#define RINGUART_BAUD_RATE 9600

void ringuartinit();
void txring_enqueue(uint8_t data); // - am ende einfügen. automatisch senden starten.
uint8_t txring_count(); // - noch zu sendende zeichen im buffer
uint8_t txring_space(); // - noch verfügbarer space im buffer

uint8_t rxring_count(); // - noch zu verarbeitende zeichen im buffer
uint8_t rxring_space(); // - noch verfügbarer space im buffer
uint8_t rxring_dequeue(); // - erstes zeichen entfernen und ausliefern

#endif /* RINGUART_H_ */
